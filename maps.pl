use strict;
use warnings;
use POSIX qw/strftime/;

#########################################################################################################
# Program: maps.pl
# Version: 2E
# Purpose: parse gene duplication events for gene family trees
# Input: config file with ordered taxa, gene family tree file
# Output: summarization of gene duplication
# Usage [maps]: perl maps.pl [config file] [tree file] [dir] [minimum bootstrap value]
# Usage [multi]: perl maps.pl [dir] [config file] [minimum bootstrap value]
# Usage: [dir] can be . for current directory, minimum bootstrap value is optional
# Usage: [multi] config file is optional, use if you want the same config file for all multiMaps items
# Usage: config files need a .list extension, tree files need a .tre or .tree extension
# Usage: multi looks for matching [name].list and [name].tree files where [name] is the same for both files
# Author: Barker Lab
# Date Modified: 2016-04-11
###########################################################################################################

sub multiMaps{
	my ($dirname,$listfile,$minimum) = @_;
	if(not defined $listfile){
		$listfile = '.';
	}
	if(not defined $dirname){
		$dirname = '.';
	}
	if(not defined $minimum){
		$minimum = 0;
	}
	print "Starting Multi-MAPS...\n";
	
	my $ii = 0;
	
	## Read through dir and locate .list files, find matching .tree files
	## Run maps.pl if both .list and .tree files exist
	## Completed processed files will show up in a dir matching the parenthetical printed after Running (...)
	opendir(my $dh, $dirname) or die("Unable to open $dirname\n");
	while(readdir $dh){
		my $tree;
		my $fn = "";
		my $listed = "";
		print "\n!! $_ !!\n";
		if($listfile =~ /([a-zA-Z0-9\-\_\. ]+)\.list$/){
			if(/([a-zA-Z0-9\-\_\. ]+)\.(tre|tree|trees)$/){
				$ii++;
				$tree = $_;
				$fn = "_$ii";
				$listed = $listfile;
			}
		}
		else{
			if(/([a-zA-Z0-9\-\_\. ]+)\.list$/){
				$listed = $_;
				if(-r "$dirname/$1.tree"){
					$tree = "$1.tree";
				}
				else{
					if(-r "$dirname/$1.tre"){
						$tree = "$1.tre";
					}
					else{
						if(-r "$dirname/$1.trees"){
							$tree = "$1.trees";
						}
					}
				}
			}
		}
		if(defined($tree)){
			if(-r "$dirname/$tree"){
				open(my $file, "<$dirname/$listed") or die("Could not open $dirname/$listed \n");  
				my @rowsplit = split("\t",<$file>);
				print "\n######\n## Running $tree ($rowsplit[0])\n######\n\n";
				my $fname = $rowsplit[0] . $fn;
				maps($listed,$tree,$dirname,$minimum,$fname); 
				print "\n$tree ($rowsplit[0]) completed!\n";
			}
		}
    }
	closedir($dh);
	print "Multi-MAPS complete!\n";	
	
}

sub maps{

	my $outdir="output";
	unless(-e "$outdir" or mkdir "$outdir"){
		print "Unable to create directory $outdir\n";
		die();
	}
	open(my $outlog, ">>$outdir/runlog.txt");
	print logIt($outlog,"\n[" . strftime('%Y-%m-%d %H:%M:%S',localtime) . "]\nStarting script...\nStarting config parsing...\n");
	
	## Set up initial input variables, script file/pathing variables
	my ($input0,$filename,$dirname,$minimum,$fname) = @_;
	if(not defined $input0 or not defined $filename){
		print logIt($outlog,"Error: Invalid arguments\n");
		die();
	}
	if(not defined $dirname){
		$dirname = '.';
	}
	if(not defined $minimum){
		$minimum = 0;
	}
	
	##declare vars
	
	##set minimum and maximum taxa to check
	my $taxmin = 5;
	my $taxmax = 500;
	
	$input0 = "$dirname/$input0";	
	
	my $input2="$filename.modified.tree";
	my $input2b="$filename.parse.tree";
	
	my $ii = 0;
	my $il;
	my $outname;
	my @arr;
	my %outfile;
	my $file;
	my $file2;
	my $out2;
	my $input3;
	my $input4;
	my $input5;
	my $outout;
	my $inout;
	my $le = "B";
	
	## Read config file, set up dirname and output files, check against min and max taxa counts
	unless(open($file, "<$input0")) {
		print logIt($outlog,"Error: Could not open .list file ($input0)\n");
		die();
	}
	while (my $row = <$file>) {
		chomp $row;
		$row =~ s/^\s+|\s+$//g;
		my @rowsplit = split("\t",$row);
		my $splitcount = @rowsplit;
		if(($splitcount - 1) < $taxmin or ($splitcount - 1) > $taxmax){
			print logIt($outlog,"Error: Taxa count is outside of allowed range ($taxmin, $taxmax)\n");
			die();			
		}
	   	foreach(@rowsplit){
	   		if(not defined $outname){
				if(defined $fname){
					$outname = $fname;
				}
				else{
					$outname = $_;
				}
	   		}
	   		else{
	   			my $dupes = () = $row =~ /\t$_(\t|$)/g;
	   			if($dupes > 1){
					print logIt($outlog,"Error: Repeat taxa ($_) detected\n");
					die();		   				
	   			}
	       		$arr[$ii] = $_;
	       		if($ii > 1){
	       			$le++;
	       			$outfile{$ii} = "N" . ($ii) .  "_" . $le . ".txt";
	       			print "N" . ($ii) .  "_" . $le . ".txt\n";
	       		}
	       		$ii++;
	   		}
	   	}
	}
	$ii--;
	
	unless(-e "$outdir/$outname" or mkdir "$outdir/$outname"){
		print logIt($outlog,"Error: Unable to create directory ($outdir/$outname)\n");
		die();		
	}	

	## Double check .list file matches .tree file
	print logIt($outlog,"Validating .tree file...\n");
	foreach(@arr){
		unless(open($file2,"<$dirname/$filename")){
			print logIt($outlog,"Error: Missing .tree file ($dirname/$filename)\n");
			die();
		}
		my $isThere = 0;
		THERE: {
			while(my $row = <$file2>){
				if($row =~ /($_)/){
					$isThere = 1;
					last THERE;
				}
			}
		}
		close($file2);
		if($isThere == 0){
			print logIt($outlog,"Error: Unable to locate $_ in .list file\n");
			die();
		}		
	}
	
	print logIt($outlog,"Config parsing completed!\nBeginning tree cleanup...\n");
	
	## Regex replace to remove extraneous information from the .tree file
	unless(open($file2,"<$dirname/$filename")){
		print logIt($outlog,"Error: Missing .tree file ($dirname/$filename)\n");
		die();
	}
	unless(open($out2,">$outdir/$outname/$input2")){
		print logIt($outlog,"Error: Unable to create file ($outdir/$outname/$input2)\n");
		die();
	}
	

	
	while (<$file2>){ 
		chomp $_;
		my $skip = 0;
		if($minimum > 0){
			while(/\[([0-9]{1,})\]/g){
				if($1 < $minimum){
					$skip = 1; 
				}
			}
		}
		if($skip == 0 || $minimum == 0){
			s/([a-zA-Z]{3,4})(\|[a-zA-Z]{3,4}[0-9]*x:[0-9].[0-9]*)/$1/g;
			s/:[0-9].[0-9]*//g;
			s/e-//g;
			s/[0-9]*//g;
			s/\|\w{3}//g;
			s/\.//g;
			s/\[\]//g;
			print $out2 "$_\n";
		}
	}
	close $file2;
	close $out2;
	
	print logIt($outlog,"Tree cleanup completed!\nBeginning outfile processing...\n");
	
	##Start main outfile processing
	unless(open($input3,"<$outdir/$outname/$input2")){
		print logIt($outlog,"Error: Missing file ($input2)\n");
		die();
	}
	unless(open($input4,">$outdir/$outname/$input2b")){
		print logIt($outlog,"Error: Unable to open file ($outdir/$outname/$input2b)\n");
		die();
	}
	
	for(my $ia=2;$ia<$ii;$ia++){
		my $outed;
		unless(open($outed, ">$outdir/$outname/$outfile{$ia}")){
			print logIt($outlog,"Error: Unable to open file ($outdir/$outname/$outfile{$ia})\n");
			die();
		}
		print $outed "phylo_N" . $ia . "_" . chr(65 + $ia) . "\t2x\t4x\n";
		close $outed;
	}
	
	my $ib = 1;
	
	while(<$input3>){
		chomp($_);
		for(my $ib = 1;$ib <= $ii + 1; $ib++){
			my $cchar = $arr[$ib - 1];
			my $dchar = chr(64 + $ib);
	 		s/$cchar/$dchar/g;
		}
		print $input4 "$_\n";
	}
	close ($input3);
	close ($input4);
	
	unless(open($input5,"<$outdir/$outname/$input2b")){
		print logIt($outlog,"Error: Missing file ($outdir/$outname/$input2b)\n");
		die();
	}
	
	## Go through each taxa and count events
	while (<$input5>) {
	   if (/\(/){
	        chomp $_;
	        my $input = $_;
			my $il = 'A';
	        for(my $ic = 1; $ic <= $ii+1; $ic++){
				while ($input=~/\($il,$il\)/){
					$input=~s/\($il,$il\)/$il/g;
				}
				$il++;
	        }
	
	        my $var1 = "A";
	        my $var2 = "B";
	        my $var3 = "N2";
	        my $var4 = "C";
	        my $count1;
	        my $count2;
	        my $count3;
	        my $count4;
	        
	        for(my $id = 2; $id <= $ii; $id++){
	        	my $outed;
	        	if($id > 2){
	        		$input=~s/$var1/$var3/g;
	               	$input=~s/$var2/$var3/g;
	               	while ($input=~/\($var3,$var3\)/) {$input=~s/\($var3,$var3\)/$var3/g};
	        		$var1 = "N" . ($id - 1);
	        		$var2++;
	        		$var3 = "N$id";
	        		$var4++;

	        	}
	        	$input=~s/\($var1,$var2\)/$var3/g;
	            $input=~s/\($var2,$var1\)/$var3/g;
	            $input=~s/\($var3,$var1\)/\($var3,$var3\)/g;
	            $input=~s/\($var3,$var2\)/\($var3,$var3\)/g;
	            $input=~s/\($var1,$var3\)/\($var3,$var3\)/g;
	            $input=~s/\($var2,$var3\)/\($var3,$var3\)/g;
	            $input=~s/\(\($var3,$var3\),$var1\)/\(\($var3,$var3\),$var3\)/g;
	            $input=~s/\(\($var3,$var3\),$var2\)/\(\($var3,$var3\),$var3\)/g;
	            $input=~s/\($var1,\($var3,$var3\)\)/\($var3,\($var3,$var3\)\)/g; 
	            $input=~s/\($var2,\($var3,$var3\)\)/\($var3,\($var3,$var3\)\)/g;
	            $count1=($input =~ s/$var3,$var4/$var3,$var4/g);
	            $count2=($input =~ s/$var4,$var3/$var4,$var3/g);
	            $count3=($input =~ s/\($var3,$var3\),$var4/\($var3,$var3\),$var4/g);
	            $count4=($input =~ s/$var4,\($var3,$var3\)/$var4,\($var3,$var3\)/g);
	            unless(open($outed, ">>$outdir/$outname/$outfile{$id}")){
	            	print logIt($outlog,"Error: Unable to open file ($outdir/$outname/$outfile{$id})\n");
	            	die();
	            }
	            print $outed "$input\t" . ($count1 + $count2) . "\t" . ($count3 + $count4) . "\n";
	            close($outed);
	        }
	
		}
	
	}
	close($input5);
	print logIt($outlog,"Outfile processing completed!\nBeginning result summary export...\n");
	
	unless(open($outout,">$outdir/$outname/$outname.csv")){
		print logIt($outlog,"Error: Unable to open file ($dirname/$outname/$outname.csv)\n");
		die();
	}
	print $outout "MRCA,Non-duplication Per,Duplication Per,Non-duplication,Duplication,Total\n";
	
	## Read through outfiles, count duplication events
	for(my $ie = 2; $ie <= $ii; $ie++){
		my $sum=0;
		my $sum2=0;
		my $sumsum=0;
		my $skip=0;
		my %li;
		unless(open($inout,"<$outdir/$outname/$outfile{$ie}")){
			print logIt($outlog,"Error: Unable to open file ($outdir/$outname/$outfile{$ie})\n");
			die();
		} 
		while(<$inout>){
			chomp $_;
			if($skip == 0){
				$li{'fn'} = "$dirname/$filename";
				$li{'fo'} = "$outdir/$outname/N$ie" . "_genetree.txt";
			}
			else{
				my @ID = split(/\t/, $_, 4);
				$sum += $ID[1];
				$sum2 += $ID[2];
				if($ID[2] > 0){
					$li{$skip+1} = 1;					
				}
			}
			$skip++;
		}
		my $rl = readLines(%li);
		if($rl =~ /^Error/){
			print logIt($outlog,$rl);
		}
		$sumsum = ($sum+$sum2 > 0) ? ($sum/($sum+$sum2)) : 0;	
		print $outout "N$ie," . ($sumsum*100) . "\%," . ((1-$sumsum)*100) . "\%,$sum,$sum2," . ($sum+$sum2) . "\n"; 
		close($inout);
	}
	close($outout);
	print logIt($outlog,"Result summary export completed!\nScript complete!\n");	
}

sub logIt{
		my($logger,$logstr) = @_;
		print $logger $logstr;
		return $logstr;
}

sub readLines{
	my (%loi) = @_;
	my $fh;
	my $fou;
	
	my $fn = $loi{'fn'};
	my $fo = $loi{'fo'};
	
	
	unless(open($fh,"<$fn")){
		return "Error: Unable to open file ($fn)\n";
	}
	unless(open($fou,">$fo")){
		return "Error: Unable to open file ($fo)\n";		
	}
	
	my $ln = (keys %loi) - 2;
	
	while (<$fh>) {
	   if ($loi{$.}) {
	      print $fou "\[$.\]$_";
	      last unless --$ln;
	   }   
	}
	return "Success";
}

## Figure out if user wants to run maps or multi_maps
my ($first, $second, $third) = @ARGV;
if(defined($first) && defined($second) && $first =~ /\.list$/i && $second =~ /\.tree$|.tre$/i ){
	maps(@ARGV);	
}
else{
	multiMaps(@ARGV);
}